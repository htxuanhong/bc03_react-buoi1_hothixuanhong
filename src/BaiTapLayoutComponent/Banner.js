import React, { Component } from 'react'

export default class Banner extends Component {
    render() {
        return (
            <header class="py-4">

                <div class="p-3 bg-light rounded-3 text-left">
                    <div class="m-4 ">
                        <h1 class="display-5 fw-bold">A warm welcome!</h1>
                        <p class="fs-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam minus inventore, velit quae reprehenderit fugit. Laboriosam libero quo maiores quibusdam cumque, adipisci odio cupiditate! Harum laborum nostrum minima tempore quam?</p>
                        <a class="btn btn-primary btn-lg" href="#!">Call to action</a>
                    </div>
                </div>

            </header>
        )
    }
}
