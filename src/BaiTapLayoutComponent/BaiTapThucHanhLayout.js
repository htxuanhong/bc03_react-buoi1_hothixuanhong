import React, { Component } from 'react'
import Banner from './Banner'
import Footer from './Footer'
import Header from './Header'
import Item from './Item'

export default class BaiTapThucHanhLayout extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className='container'>
                    <Banner />

                    <div className='row '>
                        <Item description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente asse necessitatibus neque." />
                        <Item description="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exlocabo magni sapiente, tempore debitis beatae culpa natus architecto." />
                        <Item description="Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente asse necessitatibus neque." />
                        <Item description="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exlocabo magni sapiente, tempore debitis beatae culpa natus architecto." />

                    </div>
                </div>

                <Footer />
            </div>
        )
    }
}

