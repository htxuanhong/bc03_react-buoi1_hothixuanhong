import React, { Component } from 'react';

export default class Item extends Component {
    render() {

        return (
            <div class="col-lg-3 col-xxl-4 mb-5">
                <div class="card  border-0 h-100">
                    <img class="card-img-top" src="./hinhanh/hinhanh.jpg" alt="" />
                    <div class="card-body text-center p-4 p-lg-3 pt-0 pt-lg-0">
                        <div
                            class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4"
                        >
                            <i class="bi bi-collection"></i>
                        </div>
                        <h2 class="fs-4 fw-bold">Card title</h2>
                        <p class="mb-0 ">
                            {this.props.description}
                        </p>

                    </div>
                    <div class="py-3 mt-2 bg-light border-top">
                        <button href="#" class="btn btn-primary">Find Out More!</button>
                    </div>
                </div>
            </div >


        )
    }
}
